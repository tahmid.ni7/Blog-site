# Blog-site
It is a complete personal dynamic blog or blogsite project in PHP with relational database. It is a dynamic project. It allows multiple user login system. Here admin manage all the things and user can like & comment in each content after creating their account.

Functionalities: Based on CMS
- Registration
- Create page
- Dynamic menu
- Multiple login

For better help and using the project please read the "read me.txt" file.

Thanks from Tahmid Nishat.

